=============
django-pghero
=============

Postgres insights made easy. Django port of: https://github.com/ankane/pghero

------------
Requirements
------------

* `Django <https://www.djangoproject.com/>`_

------------
Installation
------------

* Install ``django-pghero``:

.. code-block:: python

    pip install django-pghero

* Add ``django_pghero`` to ``INSTALLED_APPS`` in ``settings.py``:

.. code-block:: python

    INSTALLED_APPS = (
        # other apps
        'django_pghero',
    )

* Add these lines to your URL configuration, urls.py:

.. code-block:: python

    urlpatterns += patterns('',
        (r'^admin/pghero/', include('django_pghero.urls')),
    )

-----------
Inspiration
-----------

`django-pghero` was heavilty influenced by:

* `pghero <https://github.com/ankane/pghero>`_: Direct port
