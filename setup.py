from setuptools import setup


setup(
    name='django-pghero',
    version='0.0.0',
    author='Jeff Triplett',
    author_email='jeff.triplett@gmail.com',
    url='https://gitlab.com/jeff.triplett/django-pghero',
    license='BSD',
    description='Postgres insights made easy. Django port of: https://github.com/ankane/pghero',
    long_description=open('README.rst').read(),
    zip_safe=False,
    include_package_data=True,
    packages=[
        'django_pghero',
    ],
    package_data={'': ['README.rst']},
    install_requires=[
        'Django>=1.6',
    ],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ]
)
