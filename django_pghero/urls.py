from django.conf.urls import patterns, url

from . import views


urlpatterns = patterns(
    '',
    url(r'^$', views.PgHeroIndexView.as_view(), name='pg_hero_index'),
    url(r'^indexes/$', views.IndexView.as_view(), name='pg_hero_indexes'),
    url(r'^queries/$', views.QueryView.as_view(), name='pg_hero_queries'),
    url(r'^spaces/$', views.SpaceView.as_view(), name='pg_hero_spaces'),
    url(r'^status/$', views.StatusView.as_view(), name='pg_hero_status'),
    url(r'^tune/$', views.TuneView.as_view(), name='pg_hero_tune'),
)
