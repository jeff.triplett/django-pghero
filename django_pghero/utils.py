from django.db import connection


class PgHero(object):

    def __init__(self):
        self.cursor = connection.cursor()

    def dictfetchall(self, cursor):
        """Returns all rows from a cursor as a dict"""

        desc = cursor.description
        return [
            dict(zip([col[0] for col in desc], row))
            for row in cursor.fetchall()
        ]

    def database_size(self):
        sql = """
        SELECT pg_size_pretty(pg_database_size(current_database()));
        """
        self.cursor.execute(sql)
        return self.cursor.fetchone()[0]

    def index_hit_rate(self):
        sql = """
            SELECT
                (sum(idx_blks_hit)) / nullif(sum(idx_blks_hit + idx_blks_read),0) AS rate
            FROM
                pg_statio_user_indexes;
        """
        self.cursor.execute(sql)
        try:
            return self.cursor.fetchone()[0]
        except IndexError:
            return None

    def index_usage(self):
        sql = """
            SELECT
                relname AS table,
                CASE idx_scan
                    WHEN 0 THEN 'Insufficient data'
                    ELSE (100 * idx_scan / (seq_scan + idx_scan))::text
                END percent_of_times_index_used,
                n_live_tup rows_in_table
            FROM
                pg_stat_user_tables
            ORDER BY
                n_live_tup DESC,
                relname ASC;
        """
        self.cursor.execute(sql)
        return self.dictfetchall(self.cursor)

    def kill(self, pid):
        sql = """
        SELECT pg_cancel_backend(#{pid.to_i})").first["pg_cancel_backend"] == "t";
        """
        self.cursor.execute(sql)
        return self.dictfetchall(self.cursor)

    def kill_all(self):
        sql = """
          SELECT
              pg_terminate_backend(pid)
          FROM
              pg_stat_activity
          WHERE
              pid <> pg_backend_pid()
              AND query <> '<insufficient privilege>';
        """
        self.cursor.execute(sql)
        return self.dictfetchall(self.cursor)

    def locks(self):
        sql = """
            SELECT DISTINCT ON (pid)
                pg_stat_activity.pid,
                pg_stat_activity.query,
                age(now(), pg_stat_activity.query_start) AS age
            FROM
                pg_stat_activity
            INNER JOIN
                pg_locks ON pg_locks.pid = pg_stat_activity.pid
            WHERE
                pg_stat_activity.query <> '<insufficient privilege>'
                AND pg_locks.mode = 'ExclusiveLock'
                AND pg_stat_activity.pid <> pg_backend_pid()
            ORDER BY
                pid,
                query_start;
        """
        self.cursor.execute(sql)
        return self.dictfetchall(self.cursor)

    def long_running_queries(self):
        sql = """
          SELECT
              pid,
              state,
              application_name AS source,
              age(now(), xact_start) AS duration,
              waiting,
              query,
              xact_start AS started_at
          FROM
              pg_stat_activity
          WHERE
              query <> '<insufficient privilege>'
              AND state <> 'idle'
              AND pid <> pg_backend_pid()
              AND now() - query_start > interval '5 minutes'
          ORDER BY
              query_start DESC;
          """
        self.cursor.execute(sql)
        return self.dictfetchall(self.cursor)

    def missing_indexes(self):
        sql = """
            SELECT
                relname AS table,
                CASE idx_scan
                    WHEN 0 THEN 'Insufficient data'
                    ELSE (100 * idx_scan / (seq_scan + idx_scan))::text
                END percent_of_times_index_used,
                n_live_tup rows_in_table
            FROM
                pg_stat_user_tables
            WHERE
                idx_scan > 0
                AND (100 * idx_scan / (seq_scan + idx_scan)) < 95
                AND n_live_tup >= 10000
            ORDER BY
                n_live_tup DESC,
                relname ASC;
        """
        self.cursor.execute(sql)
        return self.dictfetchall(self.cursor)

    def relation_sizes(self):
        sql = """
            SELECT
                c.relname AS name,
                CASE WHEN c.relkind = 'r' THEN 'table' ELSE 'index' END AS type,
                pg_size_pretty(pg_table_size(c.oid)) AS size
            FROM
                pg_class c
            LEFT JOIN
                pg_namespace n ON (n.oid = c.relnamespace)
            WHERE
                n.nspname NOT IN ('pg_catalog', 'information_schema')
                AND n.nspname !~ '^pg_toast'
                AND c.relkind IN ('r', 'i')
            ORDER BY
                pg_table_size(c.oid) DESC,
                name ASC;
        """
        self.cursor.execute(sql)
        return self.dictfetchall(self.cursor)

    def running_queries(self):
        sql = """
          SELECT
              pid,
              state,
              application_name AS source,
              age(now(), xact_start) AS duration,
              waiting,
              query,
              xact_start AS started_at
          FROM
              pg_stat_activity
          WHERE
              query <> '<insufficient privilege>'
              AND state <> 'idle'
              AND pid <> pg_backend_pid()
          ORDER BY
              query_start DESC;
        """
        self.cursor.execute(sql)
        return self.dictfetchall(self.cursor)

    def settings(self):
        #values = Hash[ select_all(Connection.send(:sanitize_sql_array, ["SELECT name, setting, unit FROM pg_settings WHERE name IN (?)", names])).sort_by{|row| names.index(row["name"]) }.map{|row| [row["name"], friendly_value(row["setting"], row["unit"])] } ]

        sql = """
            SELECT
                name,
                setting,
                unit
            FROM
                pg_settings
            WHERE
                name
            IN (
                'max_connections',
                'shared_buffers',
                'effective_cache_size',
                'work_mem',
                'maintenance_work_mem',
                'checkpoint_segments',
                'checkpoint_completion_target',
                'wal_buffers',
                'default_statistics_target'
            );
        """
        self.cursor.execute(sql)
        return self.dictfetchall(self.cursor)

    def table_hit_rate(self):
        sql = """
            SELECT
                sum(heap_blks_hit) / nullif(sum(heap_blks_hit) + sum(heap_blks_read),0) AS rate
            FROM
                pg_statio_user_tables;
        """
        self.cursor.execute(sql)
        try:
            return self.cursor.fetchone()[0]
        except IndexError:
            return None

    def unused_indexes(self):
        sql = """
            SELECT
                relname AS table,
                indexrelname AS index,
                pg_size_pretty(pg_relation_size(i.indexrelid)) AS index_size,
                idx_scan as index_scans
            FROM
                pg_stat_user_indexes ui
            INNER JOIN
                pg_index i ON ui.indexrelid = i.indexrelid
            WHERE
                NOT indisunique
                AND idx_scan < 50 AND pg_relation_size(relid) > 5 * 8192
            ORDER BY
                pg_relation_size(i.indexrelid) / nullif(idx_scan, 0) DESC NULLS FIRST,
                pg_relation_size(i.indexrelid) DESC,
                relname ASC;
        """
        self.cursor.execute(sql)
        return self.dictfetchall(self.cursor)

    def unused_tables(self):
        sql = """
            SELECT
                relname AS table,
                n_live_tup rows_in_table
            FROM
                pg_stat_user_tables
            WHERE
                idx_scan = 0
            ORDER BY
                n_live_tup DESC,
                relname ASC;
        """
        self.cursor.execute(sql)
        return self.dictfetchall(self.cursor)
