from django.conf import settings
from django.contrib import admin


SHOW_ADMIN_LINK = getattr(settings, 'PG_HERO_SHOW_ADMIN_LINK', True)  # False


if SHOW_ADMIN_LINK:
    admin.site.index_template = 'pghero/index.html'
