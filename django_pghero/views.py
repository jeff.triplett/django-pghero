from django.db import transaction
from django.db.utils import DatabaseError
from django.views.generic import TemplateView

from django_pghero.utils import PgHero


class PgHeroIndexView(TemplateView):
    template_name = 'pghero/index.html'


class IndexView(TemplateView):
    template_name = 'pghero/indexes.html'

    def get_context_data(self):
        pg_hero = PgHero()

        context = super(IndexView, self).get_context_data()
        context.update({
            'index_usage': pg_hero.index_usage(),
        })
        return context


class QueryView(TemplateView):
    template_name = 'pghero/queries.html'

    def get_context_data(self):
        pg_hero = PgHero()

        running_queries_error = False
        try:
            running_queries = pg_hero.running_queries()
        except DatabaseError:
            running_queries = []
            transaction.rollback()
            running_queries_error = True

        context = super(QueryView, self).get_context_data()
        context.update({
            'running_queries': running_queries,
            'running_queries_error': running_queries_error,
        })
        return context


class SpaceView(TemplateView):
    template_name = 'pghero/spaces.html'

    def get_context_data(self):
        pg_hero = PgHero()

        context = super(SpaceView, self).get_context_data()
        context.update({
            'database_size': pg_hero.database_size(),
            'relation_sizes': pg_hero.relation_sizes(),
        })
        return context


class StatusView(TemplateView):
    template_name = 'pghero/statuses.html'

    def get_context_data(self):
        pg_hero = PgHero()
        locks = pg_hero.locks()
        table_hit_rate = pg_hero.table_hit_rate()
        index_hit_rate = pg_hero.index_hit_rate()
        good_cache_rate = table_hit_rate >= 0.99 and index_hit_rate >= 0.99

        long_running_queries_error = False
        try:
            long_running_queries = pg_hero.long_running_queries()
        except DatabaseError:
            long_running_queries = []
            transaction.rollback()
            long_running_queries_error = True

        context = super(StatusView, self).get_context_data()
        context.update({
            'locks': locks,
            'long_running_queries': long_running_queries,
            'long_running_queries_error': long_running_queries_error,
            'index_hit_rate': index_hit_rate * 100,
            'table_hit_rate': table_hit_rate * 100,
            'good_cache_rate': good_cache_rate,
            'missing_indexes': pg_hero.missing_indexes(),
            'unused_indexes': pg_hero.unused_indexes(),
            'unused_tables': pg_hero.unused_tables(),
        })
        return context


class TuneView(TemplateView):
    template_name = 'pghero/tune.html'

    def get_context_data(self):
        pg_hero = PgHero()
        context = super(TuneView, self).get_context_data()
        context.update({
            'settings': pg_hero.settings(),
        })
        return context
