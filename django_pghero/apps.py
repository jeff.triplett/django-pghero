from django.apps import AppConfig


class PgHeroConfig(AppConfig):

    name = 'django_pghero'
    verbose_name = 'PG Hero'
